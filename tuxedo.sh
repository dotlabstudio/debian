#!/usr/bin/env bash

echo "post install script for tuxedo os "

sudo apt install -y fish plasma-workspace-wayland yt-dlp tldr hunspell hunspell-en-au k3b libk3b8-extracodecs lynis clamav

echo "setup tldr"
tldr --update

echo " "
echo "harden with lynis"

sudo apt install -y libpam-tmpdir apt-listchanges needrestart fail2ban debsums apt-show-versions sysstat auditd

echo "copy fail2ban config to local"
sudo cp /etc/fail2ban/jail.conf  /etc/fail2ban/jail.local 

echo " "
echo "setup codecs"
sudo apt install -y kubuntu-restricted-extras kubuntu-restricted-addons

echo " "
echo "setup tailscale"
curl -fsSL https://tailscale.com/install.sh | sh

echo "don't forget to setup the firewall and ssh server"

echo "setup fish"
chsh -s /usr/bin/fish

