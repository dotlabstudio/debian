#!/usr/bin/env bash

echo "This script will setup a debian server with my cli tools, firewall, ssh server, tailscale, crowdsec,"
echo "virtual machines, unattended upgrades, cockpit and lastly an antivirus scanner"

echo "update system first"
sudo apt update && sudo apt upgrade -y

echo "install unattended upgrades if it was not selected during the installation process"
sudo apt install unattended-upgrades -y

echo "update unattended upgrades and copy over with my configs"

sudo cp 55unattended-upgrades /etc/apt/apt.conf.d/55unattended-upgrades

echo "install ssh server"
sudo apt install openssh-server -y

# check status of ssh server
# sudo systemctl status ssh

# start ssh server
#sudo systemctl start sshd

# enable ssh server at boot
sudo systemctl enable --now ssh

echo "update ssh server configs and copy over with my configs"

sudo cp hardenSSH.conf /etc/ssh/sshd_config.d/hardenSSH.conf

sudo cp issue.net /etc/issue.net
sudo cp issue /etc/issue

echo "create a ssh group and add primary user to the group"
sudo groupadd sshusers
sudo usermod -a -G sshusers $USER

echo "install my cli tools"
sudo apt install htop ripgrep neofetch exa bat fish vim yt-dlp tealdeer lsd fonts-jetbrains-mono fonts-font-awesome fonts-powerline fonts-comic-neue fonts-firacode ttf-mscorefonts-installer curl wget git locate p7zip p7zip-full unzip preload tlp tlp-rdw rsync -y

echo "install more essentail tools"
#from
# https://kskroyal.com/thingsafterdebianlinux/
sudo apt install linux-headers-$(uname -r) firmware-linux firmware-linux-nonfree vulkan-tools vulkan-validationlayers unrar gstreamer1.0-vaapi clang cargo libc6-i386 libc6-x32 libu2f-udev samba-common-bin exfat-fuse -y


echo "setup firewall"
sudo apt install ufw -y

echo "setup firewall rules and then enable the firewall"
sudo ufw default allow outgoing comment 'allow all outgoing traffic'
sudo ufw default deny incoming comment 'deny all incoming traffic'

sudo ufw allow ssh

#sudo ufw limit in ssh comment 'allow SSH connections in'
# just double out ssh rules

# allow traffic out to port 53 -- DNS
sudo ufw allow out 53 comment 'allow DNS calls out'

# allow traffic out to port 123 -- NTP
sudo ufw allow out 123 comment 'allow NTP out'

# allow traffic out for HTTP, HTTPS, or FTP
# apt might needs these depending on which sources you're using
sudo ufw allow out http comment 'allow HTTP traffic out'
sudo ufw allow out https comment 'allow HTTPS traffic out'
sudo ufw allow out ftp comment 'allow FTP traffic out'

# allow whois
sudo ufw allow out whois comment 'allow whois'

# allow traffic out to port 68 -- the DHCP client
# you only need this if you're using DHCP
sudo ufw allow out 67 comment 'allow the DHCP client to update'
sudo ufw allow out 68 comment 'allow the DHCP client to update'

sudo systemctl reload ufw
sudo ufw enable

echo "setup tailscale"
curl -fsSL https://pkgs.tailscale.com/stable/debian/bookworm.noarmor.gpg | sudo tee /usr/share/keyrings/tailscale-archive-keyring.gpg >/dev/null

curl -fsSL https://pkgs.tailscale.com/stable/debian/bookworm.tailscale-keyring.list | sudo tee /etc/apt/sources.list.d/tailscale.list

sudo apt update

sudo apt install tailscale -y

echo "setup crowdsec"
curl -s https://packagecloud.io/install/repositories/crowdsec/crowdsec/script.deb.sh | sudo bash

sudo apt update

sudo apt install crowdsec -y 

sudo apt install crowdsec-firewall-bouncer-iptables -y

echo "setup virtual machines"
sudo apt install qemu-kvm qemu-system qemu-utils python3 python3-pip libvirt-clients libvirt-daemon-system bridge-utils virtinst libvirt-daemon -y

# check virtual machine services are running
#sudo systemctl status libvirtd.service

echo "Start Default Network for Networking for virtual machines"
sudo virsh net-start default
sudo virsh net-autostart default

echo "check virtual machine networks are running"
sudo virsh net-list --all

echo "add user to the virtual machine groups"
sudo usermod -aG libvirt $USER
sudo usermod -aG libvirt-qemu $USER
sudo usermod -aG kvm $USER
sudo usermod -aG input $USER
sudo usermod -aG disk $USER

echo "installs cokcpit with th virtual machines and podman containers modules"

echo "boost virtual machine network performance"

sudo modprobe vhost_net

echo "install nfs stuff"
sudo apt install nfs-common -y

echo "install cockpit web"

sudo apt install cockpit cockpit-machines cockpit-podman cockpit-pcp podman-compose -y

echo "start cockpit services"
sudo systemctl start cockpit
sudo systemctl enable --now cockpit.socket

echo "open firewall ports for cockpit"
sudo ufw allow 9090/tcp
sudo ufw reload

echo "harden debian with lynis"

sudo apt install libpam-tmpdir apt-listchanges apt-listbugs needrestart debsums apt-show-versions sysstat auditd rkhunter chkrootkit fail2ban lynis -y

echo "update database"

sudo updatedb

echo "copy fail2ban defaults to local"
sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
sudo cp /etc/fail2ban/fail2ban.conf /etc/fail2ban/fail2ban.local
sudo systemctl stop fail2ban
sudo systemctl disable fail2ban
sudo systemctl stop fail2ban.service
sudo systemctl disable fail2ban.service

#echo "disable services not needed"
#sudo systemctl stop auditd
#sudo systemctl disable auditd
#sudo systemctl stop auditd.service
#sudo systemctl disable auditd.service

echo "start audit services"
sudo systemctl start sysstat

sudo systemctl enable --now sysstat

echo "install firejails"
sudo apt install firejail firejail-profiles -y

# install rng tools
sudo apt install rng-tools-debian -y

echo "HRNGDEVICE=/dev/urandom" | sudo tee -a /etc/default/rng-tools-debian

sudo systemctl stop rng-tools.service
sudo systemctl start rng-tools.service

echo "install antivirus scanner"
sudo apt install clamav clamav-freshclam clamav-daemon -y
sudo service clamav-freshclam start

echo "Setup suspend stuff"
sudo mkdir -p /etc/systemd/sleep.conf.d
sudo cp nosuspend.conf /etc/systemd/sleep.conf.d/

sudo mkdir -p /etc/systemd/logind.d
sudo cp laptoplid.conf /etc/systemd/logind.d/

echo "create my directories"
cd ~
mkdir -p lab 
mkdir -p ventoyISOs
mkdir -p ventoyBurned
mkdir -p zOffline
mkdir -p zArchived

echo "Finished setting up Debian server; please reboot and setup a desktop environment"
echo " "

echo "don't forget to change shell to fish"
echo " "

echo "don't forget to connect the new machine to the Tailscale network and authenticate in the browser: "
echo "with sudo tailscale up"
echo " "

echo "don't forget to enrol with sudo cscli console enroll "
echo " "

echo "don't forget to setup unattended upgrades with"
echo "sudo dpkg-reconfigure --priority=low unattended-upgrades"



