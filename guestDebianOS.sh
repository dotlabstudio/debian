#!/usr/bin/env bash

echo " This script just installs the guest os tools under virt manager for a Debian vm"

sudo apt install spice-vdagent spice-webdavd qemu-guest-agent avahi-autoipd avahi-autoipd virtiofsd -y

sudo apt install gir1.2-spiceclientgtk-3.0 spice-client-gtk -y

echo "Finished setting Debian vm"
