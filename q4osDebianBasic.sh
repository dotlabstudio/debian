#!/usr/bin/env bash

echo "This script install the stuff for q4os Basic after server debian script"

echo "setup wayland on q4os"
sudo apt install plasma-workspace-wayland plasma-wayland-protocols kwin-wayland -y

echo "install graphical firewall and virt manager"

sudo apt install gufw virt-manager -y

echo "install flatpaks"
sudo apt install flatpak at-spi2-core  -y

# install flatpaks from flathub
#https://flathub.org/home
#
#flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
#  the above one installs flathub repos as a user
flatpak remote-add --user --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

flatpak install --user --noninteractive --assumeyes flathub io.github.vikdevelop.SaveDesktop

echo "install burning software"
sudo apt install k3b -y

echo "Finish setting up Debian like or q4os like basic desktop"
