#!/usr/bin/env bash

# script to install tailscale on Debian  from https://tailscale.com/download/linux/debian-bookworm

# install curl first
sudo apt install curl -y

# Add Tailscale’s package signing key and repository:
curl -fsSL https://pkgs.tailscale.com/stable/debian/bookworm.noarmor.gpg | sudo tee /usr/share/keyrings/tailscale-archive-keyring.gpg >/dev/null

curl -fsSL https://pkgs.tailscale.com/stable/debian/bookworm.tailscale-keyring.list | sudo tee /etc/apt/sources.list.d/tailscale.list

# Install Tailscale:
sudo apt-get update

sudo apt-get install tailscale -y

# Connect your machine to your Tailscale network and authenticate in your browser:
sudo tailscale up





