#!/usr/bin/env bash

# script for Convert MP4 to MOV
# from https://alecaddd.com/davinci-resolve-ffmpeg-cheatsheet-for-linux/
# for YouTube for a fast upload and a almost zero processing.

ffmpeg -i input.mov -vf yadif -codec:v libx264 -crf 1 -bf 2 -flags +cgop -pix_fmt yuv420p -codec:a aac -strict -2 -b:a 384k -r:a 48000 -movflags faststart output.mp4

echo "conversion done"
