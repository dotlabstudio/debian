#!/usr/bin/env bash

echo "installing my debian base"

sudo apt install fish git vim lynis kwin-wayland plasma-wayland-protocols plasma-workspace-wayland aspell-en hunspell-en-au hunspell-en-gb tealdeer yt-dlp k3b dvd+rw-tools clamav -y 

echo "setup default shell to fish"
chsh -s /usr/bin/fish

echo "setup tealdeer or tldr"
tldr --update

echo "harden spiral with lynis"
sudo apt install apt-listbugs needrestart fail2ban apt-show-versions sysstat auditd -y

echo "copy default fail2ban config to local"
sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local

echo "Spiral Linux is setup, please reboot and then run ssh server, firewall and flatpak scripts"
