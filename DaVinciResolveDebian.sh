#!/usr/bin/env bash

# this script aims to install DaVinci Resolve on Debian
# https://www.blackmagicdesign.com/ca/products/davinciresolve/

# prep work
sudo apt install libssl-dev build-essential linux-source linux-headers-generic fakeroot xorriso libapr1 libaprutil1 opencl-headers ocl-icd-libopencl1 clinfo ocl-icd-opencl-dev libfuse2 libxcb-composite0 libxcb-cursor0 libxcb-damage0 libxcb-xinerama0 libxcb-xinput0 -y

# for nvidia cards
#sudo apt install nvidia-driver nvidia-opencl-icd libcuda1 libglu1-mesa firmware-misc-nonfree nvidia-cuda-dev nvidia-cuda-toolkit -y

# For h.264 and h.265 export you also need the NVIDIA encode library:
#sudo apt install libnvidia-encode1 -y

# Create a symlink so Resolve can find CUDA.
#sudo ln -s /usr/lib/x86_64-linux-gnu/libcuda.so /usr/lib64/libcuda.so

# a reboot is recommended for the nvidia stuff

# download DaVinci Resolve

# get MakeResolveDeb Multi from 
# https://www.danieltufvesson.com/makeresolvedeb

# copy davinci zip and makeresolvedeb to the same folder

# in the same directory
unzip DaVinci_Resolve_*_Linux.zip
tar zxvf makeresolvedeb_*_multi.sh.tar.gz

# convert bundle into a deb
./makeresolvedeb_*_multi.sh DaVinci_Resolve_*_Linux.run

# install deb package
sudo dpkg -i davinci-*_amd64.deb

echo "Finished installing DaVinci Resolve on Debian. Change the shortcut to Exec=progl /opt/resolve/bin/resolve %u
if you are on AMD Pro drivers"
