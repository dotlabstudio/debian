#!/usr/bin/env bash

# script to video into high quality video file for streaming services

# ffmpeg needs to be installed and just change the input.mov and output.mp4 names

# source: https://alecaddd.com/how-to-install-davinci-resolve-on-ubuntu-and-fix-the-audio-issue/

ffmpeg -i input.mov -vf yadif -codec:v libx264 -crf 1 -bf 2 -flags +cgop -pix_fmt yuv420p -codec:a aac -strict -2 -b:a 384k -r:a 48000 -movflags faststart output.mp4

echo "Conversion done"
