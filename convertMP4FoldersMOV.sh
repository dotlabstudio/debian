#!/usr/bin/env bash

# convert all mp4 files in a folder into mov
for file in *.mp4

do

ffmpeg -i "$file" -vcodec dnxhd -acodec pcm_s16le -s 1920x1080 -r 30000/1001 -b:v 36M -pix_fmt yuv422p -f mov "${file%.*}_conv".mov

done

echo "conversion done"
