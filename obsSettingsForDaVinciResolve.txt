source : https://www.maketecheasier.com/install-configure-davinci-resolve-linux/

Set Up OBS Studio for Davinci Resolve

Inside your OBS settings, navigate to the “output” option and change the output mode to advanced.

Switch to the recording tab and change the output type to the custom output option (FFmpeg). 

Select “MOV” as the container format and “mpeg4” as a video encoder. 

In the audio encoder option, select the “pcm_s16le” option. 

