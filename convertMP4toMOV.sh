#!/usr/bin/env bash

# script to convert mp4 to mov from
# https://alecaddd.com/davinci-resolve-ffmpeg-cheatsheet-for-linux/
# to convert a MP4 file recorded from a video camera to a MOV format of the same quality that DaVinci Resolve can import and read.

ffmpeg -i input.mp4 -vcodec dnxhd -acodec pcm_s16le -s 1920x1080 -r 30000/1001 -b:v 36M -pix_fmt yuv422p -f mov output.mov

echo "conversion done"
