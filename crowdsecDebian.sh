#!/usr/bin/env bash

# This script installs the crowdsec on Debian systems

#  Install a security engine to detect the attacks:
curl -s https://packagecloud.io/install/repositories/crowdsec/crowdsec/script.deb.sh | sudo bash
sudo apt-get install crowdsec -y

# Install a bouncer to block the attacks:
sudo apt install crowdsec-firewall-bouncer-iptables -y

echo "Please Enroll your CrowdSec security engine!"


echo "Finished setting up crowdsec on Debian"
