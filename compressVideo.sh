#!/usr/bin/env bash

# script to use  FFmpeg to transcode and compress video file

# source: https://www.maketecheasier.com/install-configure-davinci-resolve-linux/

#“input.mov” is your .mov file from DaVinci Resolve, and “output.mp4” is your output .mp4 file.
# You can change these names to your preference.

ffmpeg -i input.mov -vf yadif -codec:v libx264 -crf 1 -bf 2 -flags +cgop -pix_fmt yuv420p -codec:a aac -strict -2 -b:a 384k -r:a 48000 -movflags faststart output.mp4

echo "Video compression is done"
