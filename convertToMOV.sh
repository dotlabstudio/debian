#!/usr/bin/env bash

# script to convert video into mov to use in DaVinci Resolve

# source: https://www.maketecheasier.com/install-configure-davinci-resolve-linux/

# just change the inputs and outputs names

ffmpeg -i input.mp4 -vcodec dnxhd -acodec pcm_s16le -s 1920x1080 -r 30000/1001 -b:v 36M -pix_fmt yuv422p -f mov output.mov

echo "Conversion done"
