#!/usr/bin/env bash

# This script installs cokcpit with th virtual machines and podman containers modules

# boost virtual machine network performance

sudo modprobe vhost_net

# install nfs stuff
sudo apt install nfs-common -y

# install cockpit web 

sudo apt install cockpit cockpit-machines cockpit-podman cockpit-pcp podman-compose -y

# start cockpit services
sudo systemctl start cockpit
sudo systemctl enable --now cockpit.socket

# open firewall ports
sudo ufw allow 9090/tcp
sudo ufw reload

# source: https://techviewleo.com/install-kvm-virt-manager-cockpit-debian/

echo "Finished setting up cockpit for Debian"
