#!/usr/bin/env bash

echo "Please run this script in a bash shell"

# remove old software from Debian
sudo apt remove firefox-esr gimp libreoffice-* -y

# install flatpak
sudo apt install flatpak at-spi2-core -y

# install flatpaks from flathub
#https://flathub.org/home
#
#flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak remote-add --user --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

#  the above one installs flathub repos as a user

# floorp needs to go first or last like wesnoth
flatpak install --user --noninteractive --assumeyes flathub one.ablaze.floorp

# Development
flatpak install --user --noninteractive --assumeyes flathub org.gnome.meld

# Education
flatpak install --user --noninteractive --assumeyes flathub org.kde.marble

# Graphics
flatpak install --user --noninteractive --assumeyes flathub org.blender.Blender

flatpak install --user --noninteractive --assumeyes flathub net.fasterland.converseen

flatpak install --user --noninteractive --assumeyes flathub io.gitlab.adhami3310.Converter

flatpak install --user --noninteractive --assumeyes flathub org.kde.digikam

flatpak install --user --noninteractive --assumeyes flathub org.darktable.Darktable

flatpak install --user --noninteractive --assumeyes flathub io.github.manisandro.gImageReader

flatpak install --user --noninteractive --assumeyes flathub org.gimp.GIMP

flatpak install --user --noninteractive --assumeyes flathub org.inkscape.Inkscape

flatpak install --user --noninteractive --assumeyes flathub org.kde.krita

# Internet
flatpak install --user --noninteractive --assumeyes flathub com.brave.Browser

flatpak install --user --noninteractive --assumeyes flathub org.cockpit_project.CockpitClient

flatpak install --user --noninteractive --assumeyes flathub app.drey.Dialect

flatpak install --user --noninteractive --assumeyes flathub org.filezillaproject.Filezilla

flatpak install --user --noninteractive --assumeyes flathub org.mozilla.firefox

flatpak install --user --noninteractive --assumeyes flathub fi.skyjake.Lagrange

flatpak install --user --noninteractive --assumeyes flathub org.localsend.localsend_app
# open source alternative to airdrop

flatpak install --user --noninteractive --assumeyes flathub org.getmonero.Monero

flatpak install --user --noninteractive --assumeyes flathub net.mullvad.MullvadBrowser

flatpak install --user --noninteractive --assumeyes flathub org.onionshare.OnionShare

flatpak install --user --noninteractive --assumeyes flathub org.qbittorrent.qBittorrent

# tor browser name change
#flatpak install --user --noninteractive --assumeyes flathub com.github.micahflee.torbrowser-launcher
flatpak install --user --noninteractive --assumeyes flathub org.torproject.torbrowser-launcher


# Multimedia
#flatpak install --user --noninteractive --assumeyes flathub org.ardour.Ardour

flatpak install --user --noninteractive --assumeyes flathub fr.handbrake.ghb

flatpak install --user --noninteractive --assumeyes flathub org.kde.kdenlive

flatpak install --user --noninteractive --assumeyes flathub com.obsproject.Studio

#flatpak install --user --noninteractive --assumeyes flathub org.openshot.OpenShot

#flatpak install --user --noninteractive --assumeyes flathub org.shotcut.Shotcut

flatpak install --user --noninteractive --assumeyes flathub org.strawberrymusicplayer.strawberry

flatpak install --user --noninteractive --assumeyes flathub org.tenacityaudio.Tenacity

flatpak install --user --noninteractive --assumeyes flathub org.videolan.VLC

# Office
flatpak install --user --noninteractive --assumeyes flathub org.libreoffice.LibreOffice

flatpak install --user --noninteractive --assumeyes flathub org.onlyoffice.desktopeditors

flatpak install --user --noninteractive --assumeyes flathub com.github.jeromerobert.pdfarranger

flatpak install --user --noninteractive --assumeyes flathub org.standardnotes.standardnotes

flatpak install --user --noninteractive --assumeyes flathub com.github.xournalpp.xournalpp

# Utilities
flatpak install --user --noninteractive --assumeyes flathub com.ktechpit.colorwall

flatpak install --user --noninteractive --assumeyes flathub org.cryptomator.Cryptomator

flatpak install --user --noninteractive --assumeyes flathub net.sapples.LiveCaptions

flatpak install --user --noninteractive --assumeyes flathub com.nextcloud.desktopclient.nextcloud

#flatpak install --user --noninteractive --assumeyes flathub io.github.peazip.PeaZip

flatpak install --user --noninteractive --assumeyes flathub io.github.vikdevelop.SaveDesktop

flatpak install --user --noninteractive --assumeyes flathub net.mkiol.SpeechNote

# more flatpaks to test
flatpak install --user --noninteractive --assumeyes flathub io.github.onionware_github.onionmedia

flatpak install --user --noninteractive --assumeyes flathub org.gnome.World.PikaBackup

#flatpak install --user --noninteractive --assumeyes flathub io.appflowy.AppFlowy

echo "Finished installing the most verified flatpaks for Debian and please reboot the system"
