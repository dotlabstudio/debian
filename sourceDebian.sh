#!/usr/bin/env bash

# script to show building source code using virtualbox as an example
#
# Steps to build:
# Download the source file and extract it.
# Run ./configure
# Run make
# Run sudo make install

# prep work from https://wiki.debian.org/BuildingTutorial
sudo apt install build-essential fakeroot devscripts libz-dev libssl-dev libcurl4-gnutls-dev libexpat1-dev \
gettext cmake gcc curl acpica-tools yasm xsltproc libxml2-dev libIDL-dev libvpx-dev libspng-dev libtpms-dev \
libpng-dev erlang-esdl-dev #libalien-sdl-dev-perl libghc-sdl2-dev libsdl-console-dev libsdl1.2-dev libsdl2-dev \
libxmu-dev libqt5svg5-dev #libqt5xmlpatterns5-dev libqwt-qt5-dev qt5-qmake qt5ct python3.11-full python3-monty -y

# from virtualbox 
sudo apt install acpica-tools chrpath doxygen g++-multilib libasound2-dev libcap-dev \
        libcurl4-openssl-dev libdevmapper-dev libidl-dev libopus-dev libpam0g-dev \
        libpulse-dev libqt5opengl5-dev libqt5x11extras5-dev qttools5-dev libsdl1.2-dev libsdl-ttf2.0-dev \
        libssl-dev libvpx-dev libxcursor-dev libxinerama-dev libxml2-dev libxml2-utils \
        libxmu-dev libxrandr-dev make nasm python3-dev python-dev-is-python3 qttools5-dev-tools \
        texlive texlive-fonts-extra texlive-latex-extra unzip xsltproc \
        \
        default-jdk libstdc++5 libxslt1-dev makeself \
        mesa-common-dev subversion yasm zlib1g-dev -y
        
sudo apt install libc6-dev-i386 lib32gcc-s1 lib32stdc++6 -y

sudo apt install pylint python3-psycopg2 python3-willow -y

# download virtualbox source code from https://www.virtualbox.org/wiki/Downloads
wget https://download.virtualbox.org/virtualbox/7.0.8/VirtualBox-7.0.8a.tar.bz2

# extract the download file
tar jxvf VirtualBox-*.tar.bz2

# cd into the extracted directory
cd VirtualBox*
./configure --disable-hardening

cd out/linux.x86/release/bin/src

make
sudo make install
cd ..

sudo modprobe vboxdrv

echo "Finished building the source code"

echo "run new code with ./VirtualBox"
