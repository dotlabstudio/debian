#!/usr/bin/env bash

# install my cli tools
sudo apt install htop ripgrep neofetch bat fish vim yt-dlp tealdeer lsd fonts-jetbrains-mono fonts-font-awesome fonts-powerline fonts-comic-neue fonts-firacode ttf-mscorefonts-installer curl wget git locate p7zip p7zip-full unzip preload tlp tlp-rdw rsync -y

# install more essentail tools from
# https://kskroyal.com/thingsafterdebianlinux/
sudo apt install linux-headers-$(uname -r) firmware-linux firmware-linux-nonfree vulkan-tools vulkan-validationlayers unrar gstreamer1.0-vaapi clang cargo libc6-i386 libc6-x32 libu2f-udev samba-common-bin exfat-fuse -y

# install gui programs

sudo apt install meld blender converseen darktable gimagereader inkscape krita filezilla onionshare qbittorrent torbrowser-launcher libavcodec-extra vlc xournalpp nextcloud-desktop k3b vorta -y

#more software to install
sudo apt install marble dialect monero wireshark-qt gnome-network-displays handbrake haruna frog minder trezor -y

echo "Finished installing my apps for Debian and please change shell to fish with chsh -s /usr/bin/fish"
