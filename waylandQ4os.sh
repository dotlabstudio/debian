#!/usr/bin/env bash

echo "Script to install wayland on Q4OS system"

sudo apt install plasma-workspace-wayland plasma-wayland-protocols kwin-wayland -y

echo "Please logout or reboot into new wayland session"
