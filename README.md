# Debian GitLab Project

This project holds my quick install scripts to setup a new Debian system up and running for me.

# Grab the ISO from the Debian web site

```
https://www.debian.org/
```

or
```
https://cdimage.debian.org/debian-cd/current/amd64/iso-dvd/
```

```
https://www.debian.org/CD/http-ftp/
```


for the live dvd iso images:

```
https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/
```

password is live for these live iso to install

# Install

Use the graphical expert install

Use most of the defaults or recommended stuff

Enable all the repos and backpack ports

# Post Install

## Edit the sources.list

```
sudo vi /etc/apt/sources/list
```

comment out the installer repo

### Add a super user if you forgot during the installation process

```
adduser $USER

usermod -aG sudo $USER
```

needs a reboot 


### Update the system

```
sudo apt update; sudo apt upgrade -y
```

### Check software sources in plasma

```
sudo software-properties-qt 
```

### Adding Contrib and Non-Free Repositories

```
sudo add-apt-repository contrib

sudo add-apt-repository non-free

sudo apt update
```

### Plasma Desktop after Server Install

```
sudo apt install kde-plasma-desktop
```

# Clone this repo with:

```
git clone https://gitlab.com/dotlabstudio/debian.git
```

then cd into the repo directory and 

```
chmod +x *.sh
```

Run the flatpaksDebian.sh script first in a bash shell:

```
./flatpaksDebian.sh
```

and use the 

```
./lynisDebian.sh
```

script last

# Change default shell to fish

Check where fish is located with
```
which fish
```
then

```
chsh -s /usr/bin/fish
```

# Setup new ssh pair keys for a new machine and copy public key to a server

To generate a new ssh pair keys on on a new machine:

```
ssh-keygen -t ed25519 -a 100
```

To copy public ssh keys to a server
```
ssh-copy-id -i ~/.ssh/id_ed25519.pub $USER@<serverIPorName>
```

## Installing the Nvidia driver

To detect nvidia drivers:
```
sudo apt install nvidia-detect 

sudo nvidia-detect
```

To install the Nvidia driver:
```
sudo apt install nvidia-driver firmware-misc-nonfree 
```

To test:
```
glxgears 
```


from https://www.learnlinux.tv/debian-12-12/

To restore open source drivers

```
sudo ddm-mx -p nvidia
```


## Setup Tailscale
Use the tailscaleDebian.sh script in this repo or

install curl first with:
```
sudo apt install curl -y
```
and then

```
curl -fsSL https://tailscale.com/install.sh | sh
```

## Secure the SSH server
copy the contents of the issue.net to /etc/issue.net

```
#################################################################
#                   _    _           _   _                      #
#                  / \  | | ___ _ __| |_| |                     #
#                 / _ \ | |/ _ \ '__| __| |                     #
#                / ___ \| |  __/ |  | |_|_|                     #
#               /_/   \_\_|\___|_|   \__(_)                     #
#                                                               #
#  You are entering into a secured area! Your IP, Login Time,   #
#   Username has been noted and has been sent to the server     #
#                       administrator!                          #
#   This service is restricted to authorized users only. All    #
#            activities on this system are logged.              #
#  Unauthorized access will be fully investigated and reported  #
#        to the appropriate law enforcement agencies.           #
#################################################################

```
from https://gist.github.com/hvmonteiro/7f897cd8ae3993195855040056f87dc6


append the lines on sshd_config in this repo to /etc/ssh/sshd_config on the server

comment out X11Forwarding yes first

```
AllowTcpForwarding no 

ClientAliveCountMax 2

Compression no

LogLevel verbose

MaxAuthTries 3

MaxSessions 2

TCPKeepAlive no

X11Forwarding no

AllowAgentForwarding no
```

# Quad9 DNS

```
https://www.quad9.net/
```

for the browsers:

```
https://dns.quad9.net/dns-query
```

To check that quad9 is setup:
```
https://on.quad9.net 
```

# Git clone lynis

```
git clone https://github.com/CISOfy/lynis
```

to run lynis after cd into the git repo:
```
sudo ./lynis audit system
```

see https://cisofy.com/documentation/lynis/get-started/#installation-git

# Setup docker on Debian

To setup docker with good file server, just use casaos

```
curl -fsSL https://get.casaos.io | sudo bash
```

from https://casaos.io/ and https://github.com/IceWhaleTech/CasaOS

then copy the 75unattended-upgrades to the right directory

```
sudo cp 75unattended-upgrades /etc/apt/apt.conf.d
```

make a docker directory and use portainer to run docker containers


```
mkdir -p docker/portainer/data
```

and just run:

```
docker compose up -d
```

## Test unattended-upgrades

Test unattended-upgrades config with a dry run

```
sudo unattended-upgrades --dry-run --debug
```

## Enable automatic updates

```
sudo dpkg-reconfigure --priority=low unattended-upgrades
```

### If the service is not allowed and started

```
sudo systemctl enable unattended-upgrades
sudo systemctl start unattended-upgrades
```

### Suspend stuff

A modern alternative approach for disabling suspend and hibernation is to create /etc/systemd/sleep.conf.d/nosuspend.conf as

file is now:
```
/etc/systemd/system/sleep.conf
```

```
[Sleep]
AllowSuspend=no
AllowHibernation=no
AllowSuspendThenHibernate=no
AllowHybridSleep=no
```

The above technique works on Debian 10 Buster and newer. See systemd-sleep.conf(5) for details.

If you just want to prevent suspending when the lid is closed you can set the following options in 
```
/etc/systemd/system/logind.conf
```


```
[Login]
HandleLidSwitch=ignore
HandleLidSwitchDocked=ignore
```

Then run 
```
sudo systemctl restart systemd-logind.service
```

or reboot.

from 
```
https://wiki.debian.org/Suspend
```


# File Sharing under KVM

sudo mount -v -t virtiofs <tag_mount> <guest_homedir>

for example:
```
sudo mount -v -t virtiofs videos ~/Videos
```

