#!/usr/bin/env bash

# install flatpaks from flathub
#https://flathub.org/home
#
#flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
#  the above one installs flathub repos as a user
flatpak remote-add --user --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

# qgis needs to be put first or last like wesnorth
#flatpak install --user --noninteractive --assumeyes flathub org.qgis.qgis
# don't really used much

# wesnorth need to be put last or first
flatpak install --user --noninteractive --assumeyes flathub org.wesnoth.Wesnoth

# floorp needs to be put first or last like wesnoth and qgis
flatpak install --user --noninteractive --assumeyes flathub one.ablaze.floorp


# Development
flatpak install --user --noninteractive --assumeyes flathub org.gnome.Boxes
# wish it would work better - virt manager still rules for me

flatpak install --user --noninteractive --assumeyes flathub org.gnome.meld

# mind mapping app
flatpak install --user --noninteractive --assumeyes flathub com.github.phase1geo.minder

#flatpak install --user --noninteractive --assumeyes flathub io.podman_desktop.PodmanDesktop
# don't really used much podman or docker on desktop

# Education
#flatpak install --user --noninteractive --assumeyes flathub com.sourcepole.kadas
# used out of date runtimes and not a verified app as yet

flatpak install --user --noninteractive --assumeyes flathub org.kde.marble

# Games

#flatpak install --user --noninteractive --assumeyes flathub hu.kramo.Cartridges

flatpak install --user --noninteractive --assumeyes flathub org.gnome.Chess

flatpak install --user --noninteractive --assumeyes flathub com.github.sakya.corechess

flatpak install --user --noninteractive --assumeyes flathub org.kde.knights

#flatpak install --user --noninteractive --assumeyes flathub net.lutris.Lutris

#flatpak install --user --noninteractive --assumeyes flathub org.supertuxproject.SuperTux

#flatpak install --user --noninteractive --assumeyes flathub net.supertuxkart.SuperTuxKart

#flatpak install --user --noninteractive --assumeyes flathub org.xonotic.Xonotic

# I'm not very good at gaming anymore

# Graphics
flatpak install --user --noninteractive --assumeyes flathub org.blender.Blender

flatpak install --user --noninteractive --assumeyes flathub net.fasterland.converseen

flatpak install --user --noninteractive --assumeyes flathub io.gitlab.adhami3310.Converter

flatpak install --user --noninteractive --assumeyes flathub org.kde.digikam

flatpak install --user --noninteractive --assumeyes flathub org.darktable.Darktable

#flatpak install --user --noninteractive --assumeyes flathub org.gnome.SimpleScan

flatpak install --user --noninteractive --assumeyes flathub io.github.manisandro.gImageReader

flatpak install --user --noninteractive --assumeyes flathub org.mattbas.Glaxnimate

flatpak install --user --noninteractive --assumeyes flathub org.gimp.GIMP

#flatpak install --user --noninteractive --assumeyes flathub org.kde.gwenview
# preinstalled

flatpak install --user --noninteractive --assumeyes flathub org.inkscape.Inkscape

flatpak install --user --noninteractive --assumeyes flathub org.kde.kcolorchooser

# kde paint app
#flatpak install --user --noninteractive --assumeyes flathub org.kde.kolourpaint
# preinstalled

flatpak install --user --noninteractive --assumeyes flathub org.kde.krita

#flatpak install --user --noninteractive --assumeyes flathub org.kde.okular
# preinstalled

# Internet
flatpak install --user --noninteractive --assumeyes flathub com.brave.Browser

flatpak install --user --noninteractive --assumeyes flathub org.cockpit_project.CockpitClient

#flatpak install --user --noninteractive --assumeyes flathub app.drey.Dialect

flatpak install --user --noninteractive --assumeyes flathub org.filezillaproject.Filezilla

flatpak install --user --noninteractive --assumeyes flathub org.mozilla.firefox

flatpak install --user --noninteractive --assumeyes flathub fi.skyjake.Lagrange

flatpak install --user --noninteractive --assumeyes flathub org.getmonero.Monero
# needs a powerful modern machine with plenty of spare cores and ram and fast ssd

flatpak install --user --noninteractive --assumeyes flathub net.mullvad.MullvadBrowser

#flatpak install --user --noninteractive --assumeyes flathub engineer.atlas.Nyxt

#flatpak install --user --noninteractive --assumeyes flathub org.onionshare.OnionShare
# use old runtimes

# another yt-dlp frontend with a name change to parabolic
flatpak install --user --noninteractive --assumeyes flathub org.nickvision.tubeconverter

#flatpak install --user --noninteractive --assumeyes flathub org.qbittorrent.qBittorrent

#flatpak install --user --noninteractive --assumeyes flathub org.qutebrowser.qutebrowser
# uses old outdated runtimes

#flatpak install --user --noninteractive --assumeyes flathub org.remmina.Remmina

#flatpak install --user --noninteractive --assumeyes flathub org.signal.Signal

#flatpak install --user --noninteractive --assumeyes flathub com.github.micahflee.torbrowser-launcher
# tor browser has change it's name to
flatpak install --user --noninteractive --assumeyes flathub org.torproject.torbrowser-launcher

#flatpak install --user --noninteractive --assumeyes flathub com.vivaldi.Vivaldi

#flatpak install --user --noninteractive --assumeyes flathub org.wireshark.Wireshark

# Multimedia
# flatpak install --user --noninteractive --assumeyes flathub org.ardour.Ardour

flatpak install --user --noninteractive --assumeyes flathub io.github.dimtpap.coppwr
# Low level control GUI for PipeWire

#flatpak install --user --noninteractive --assumeyes flathub org.gnome.NetworkDisplays
# screen casting

#flatpak install --user --noninteractive --assumeyes flathub com.dec05eba.gpu_screen_recorder
#A shadowplay-like screen recorder for Linux. The fastest screen recorder for Linux
# needs opencl cpu stuff

flatpak install --user --noninteractive --assumeyes flathub fr.handbrake.ghb

flatpak install --user --noninteractive --assumeyes flathub org.kde.haruna
# mpv kde front end

flatpak install --user --noninteractive --assumeyes flathub org.kde.kamoso

flatpak install --user --noninteractive --assumeyes flathub org.kde.kdenlive

flatpak install --user --noninteractive --assumeyes flathub org.musicbrainz.Picard

flatpak install --user --noninteractive --assumeyes flathub com.obsproject.Studio

#flatpak install --user --noninteractive --assumeyes flathub org.openshot.OpenShot

#flatpak install --user --noninteractive --assumeyes flathub org.shotcut.Shotcut

flatpak install --user --noninteractive --assumeyes flathub org.rncbc.qpwgraph

#flatpak install --user --noninteractive --assumeyes flathub org.strawberrymusicplayer.strawberry
# has clementine preinstalled

flatpak install --user --noninteractive --assumeyes flathub org.tenacityaudio.Tenacity

#flatpak install --user --noninteractive --assumeyes flathub org.videolan.VLC
# preinstalled

# Office
#flatpak install --user --noninteractive --assumeyes flathub io.appflowy.AppFlowy
# does not work on the mac

# more translate apps
#flatpak install --user --noninteractive --assumeyes flathub io.crow_translate.CrowTranslate

# ocr app
#flatpak install --user --noninteractive --assumeyes flathub com.github.tenderowl.frog

flatpak install --user --noninteractive --assumeyes flathub org.libreoffice.LibreOffice

flatpak install --user --noninteractive --assumeyes flathub org.onlyoffice.desktopeditors

flatpak install --user --noninteractive --assumeyes flathub com.github.jeromerobert.pdfarranger

flatpak install --user --noninteractive --assumeyes flathub org.standardnotes.standardnotes

flatpak install --user --noninteractive --assumeyes flathub com.github.xournalpp.xournalpp

# Settings
flatpak install --user --noninteractive --assumeyes flathub com.ktechpit.colorwall

#flatpak install --user --noninteractive --assumeyes flathub re.sonny.Junction
# Application chooser
# Junction lets you choose the application to open files and links.

# System

# system monitor
#flatpak install --user --noninteractive --assumeyes flathub io.missioncenter.MissionCenter
# not really needed

#flatpak install --user --noninteractive --assumeyes flathub io.github.flattool.Warehouse
#Manage all things Flatpak
#Warehouse is an app that manages installed Flatpaks, their user data, and Flatpak remotes.

# Utilities
#flatpak install --user --noninteractive --assumeyes flathub com.usebottles.bottles

flatpak install --user --noninteractive --assumeyes flathub org.cryptomator.Cryptomator

flatpak install --user --noninteractive --assumeyes flathub io.exodus.Exodus

# clean up app
#flatpak install --user --noninteractive --assumeyes flathub io.github.giantpinkrobots.flatsweep

# screen recording
flatpak install --user --noninteractive --assumeyes flathub io.github.seadve.Kooha

flatpak install --user --noninteractive --assumeyes flathub net.sapples.LiveCaptions

flatpak install --user --noninteractive --assumeyes flathub org.localsend.localsend_app
# open source alternative to airdrop

# Shazam like app
#flatpak install --user --noninteractive --assumeyes flathub io.github.seadve.Mousai

flatpak install --user --noninteractive --assumeyes flathub com.nextcloud.desktopclient.nextcloud

#flatpak install --user --noninteractive --assumeyes flathub io.github.onionware_github.onionmedia
# only works in nixos well and there use resume function or recovery after a crash. Better off using yt-dlp now.

#flatpak install --user --noninteractive --assumeyes flathub io.github.peazip.PeaZip

# install full codecs for flatpaks - should be installed with above applications
#flatpak install --user --noninteractive --assumeyes flathub org.freedesktop.Platform.ffmpeg-full

#flatpak install --user --noninteractive --assumeyes flathub org.gnome.World.PikaBackup

#flatpak install --user --noninteractive --assumeyes flathub io.github.vikdevelop.SaveDesktop

#flatpak install --user --noninteractive --assumeyes flathub com.borgbase.Vorta

# Lost & found

#flatpak install --user --noninteractive --assumeyes flathub net.mkiol.SpeechNote
# needs a fast modern machine to run on

# more new flatpaks to test

echo "Finished installing flatpaks for Spiral Linux system"
