#!/usr/bin/env bash

echo "This script will setup a ssh server for a mx linux fresh system"

sudo apt install openssh-server -y

echo "open firewall for ssh server"

sudo ufw allow ssh

echo "start and enable ssh server on boot"

sudo service ssh start

echo "Finished setting ssh server on mx linux"
