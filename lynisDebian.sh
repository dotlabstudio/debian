#!/usr/bin/env bash

# script to improve overall security of debian

sudo apt install libpam-tmpdir apt-listchanges apt-listbugs needrestart debsums apt-show-versions sysstat auditd rkhunter chkrootkit fail2ban -y

# update database

sudo updatedb

# copy fail2ban defaults to local
sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
sudo cp /etc/fail2ban/fail2ban.conf /etc/fail2ban/fail2ban.local
#sudo systemctl stop fail2ban
#sudo systemctl disable fail2ban
#sudo systemctl stop fail2ban.service
#sudo systemctl disable fail2ban.service

#echo "disable services not needed"
#sudo systemctl stop auditd
#sudo systemctl disable audited
#sudo systemctl stop auitd.service
#sudo systemctl disable auditd.service


# start audit services
sudo systemctl start sysstat

sudo systemctl enable --now sysstat

# sandbox with firejails
sudo apt install firejail firejail-profiles -y

# install rng tools
sudo apt install rng-tools-debian -y

echo "HRNGDEVICE=/dev/urandom" | sudo tee -a /etc/default/rng-tools-debian

sudo systemctl stop rng-tools.service
sudo systemctl start rng-tools.service

# install antivirus scanner
sudo apt install clamav clamav-freshclam clamav-daemon -y
sudo service clamav-freshclam start

echo "Finished installing security stuff. Please re-run test again"
