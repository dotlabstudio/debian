#!/usr/bin/env bash

# script to install virtualbox on Debian

# prep work
# source: https://wiki.debian.org/VirtualBox
sudo apt install build-essential dkms gcc make perl linux-headers-generic -y

# download and run virtualbox installer from flatpak remote-add --user --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo


# https://www.virtualbox.org/wiki/Linux_Downloads

wget https://download.virtualbox.org/virtualbox/7.0.8/VirtualBox-7.0.8-156879-Linux_amd64.run

chmod +x VirtualBox-*-Linux_amd64.run

sudo ./VirtualBox-*-Linux_amd64.run

# add user to vboxusers group
sudo usermod -a -G vboxusers $USER

# Load the VirtualBox kernel module
sudo modprobe vboxdrv

# download and run VM VirtualBox Extension Pack
wget https://download.virtualbox.org/virtualbox/7.0.8/Oracle_VM_VirtualBox_Extension_Pack-7.0.8.vbox-extpack


echo "Finished setting up virtualbox on Debian" 

echo "Remember to install the extension pack from File --> Tools --> Extension Pack Manager"

echo "Please reboot"
