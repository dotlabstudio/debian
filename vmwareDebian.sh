#!/usr/bin/env bash

# this script installs vmware workstation pro on Debian

sudo apt install build-essential gcc perl bzip2 dkms make linux-headers-$(uname -r) -y

# download and run vmware installer from
# https://www.vmware.com/products/workstation-pro/workstation-pro-evaluation.html
wget https://download3.vmware.com/software/WKST-1702-LX/VMware-Workstation-Full-17.0.2-21581411.x86_64.bundle

chmod +x VMware-Workstation-Full-*.x86_64.bundle

sudo ./VMware-Workstation-Full-*.x86_64.bundle

# installs vmware tools to manage guest os
sudo apt install open-vm-tools-desktop -y

# source : https://wiki.debian.org/VMware
# to enter serial number
# /usr/lib/vmware/bin/vmware-vmx --new-sn 123-123-123-123...123

# vmware-netcfg (Virtual Network Editor) with VMware Player
#ln -s /usr/lib/vmware/bin/appLoader vmware-netcfg
#ln -s /usr/lib/vmware/bin/vmware-netcfg /usr/bin/vmware-netcfg

echo "Finished setting up vmware. Enter the serial number for it"
