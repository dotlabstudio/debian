#!/usr/bin/env bash

echo "removing firefox-esr and libreoffice deb packages from a Debian system"

#if [ $SHELL -eq /usr/bin/bash]
#then
#     echo "shell is already bash"
#
#else 
#    echo "switching to the bash shell"
#    bash
#fi

sudo apt remove firefox-esr -y

echo "switching to bash"
bash 

sudo apt remove libreoffice-* -y
