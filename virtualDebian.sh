#!/usr/bin/env bash

# This script install virt manager for virtual machines on Debian from https://christitus.com/vm-setup-in-linux/

# Install QEMU and Virtual Machine Manager

sudo apt install qemu-kvm qemu-system qemu-utils python3 python3-pip libvirt-clients libvirt-daemon-system bridge-utils virtinst libvirt-daemon virt-manager -y

# check virtual machine services are running
#sudo systemctl status libvirtd.service

# Start Default Network for Networking for virtual machines
sudo virsh net-start default
sudo virsh net-autostart default

# check virtual machine networks are running
sudo virsh net-list --all

# add user to the virtual machine groups
sudo usermod -aG libvirt $USER
sudo usermod -aG libvirt-qemu $USER
sudo usermod -aG kvm $USER
sudo usermod -aG input $USER
sudo usermod -aG disk $USER

echo "Finished setting up virt manager for Debian. Please reboot"
