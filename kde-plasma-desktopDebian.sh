#!/usr/bin/env bash

echo "This script will install KDE Plasma Desktop and minimal set of applications"
echo " "
echo "This script should be run after the serverDebian.sh script"

echo " "
echo "first update system"
sudo apt update && sudo apt upgrade

echo "install graphical firewall and virt manager"
sudo apt install gufw virt-manager -y

echo "installing brave browser"

sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg

echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list

sudo apt update -y

sudo apt install brave-browser -y

echo "update unattended upgrades and copy over with my configs"

sudo cp 65unattended-upgrades /etc/apt/apt.conf.d/65unattended-upgrades

echo " "
echo "install gui apps"

sudo apt install meld blender converseen darktable gimagereader inkscape krita filezilla onionshare qbittorrent torbrowser-launcher ardour libavcodec-extra vlc xournalpp nextcloud-desktop k3b vorta -y

echo "Finished installing setting up KDE Plasma"
echo " "
echo "don't to run the flatpaks scripts"


