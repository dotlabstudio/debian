#!/usr/bin/env bash


echo "Script to uninstall cockpit from the system"

sudo systemctl stop cockpit 
sudo systemctl disable cockpit 

sudo systemctl stop cockpit.socket 
sudo systemctl disable cockpit.socket 

sudo apt remove cockpit* -y

echo "Cockpit is uninstalled"
